<?php
/**
 * View Loader
 * ----------------------
 * Allows for loading views, injecting vars into them, and 
 * either displaying them instantly or returning the contents.
 *
 * @package   skunkbad/view-loader
 * @author    Robert B Gottier
 * @copyright (c) 2019, Robert B Gottier (https://brianswebdesign.com)
 * @license   BSD - http://www.opensource.org/licenses/BSD-3-Clause
 */

namespace Skunkbad\ViewLoader;

class View {

	/**
	 * List of paths to load views from.
	 * Internal loader selects the first path with file that exists.
	 * Paths that are loaded with addPath are prepended to the array.
	 */
	protected $viewPaths = [];

	// View data
	protected $_viewData = [];	

	// --------------------------------------------------------------------

	/**
	 * Load a View from the filesystem
	 */
	public function load( $view, $viewData = [], $return = FALSE )
	{
		$out = $this->_load([ 
			'_view'   => $view, 
			'_vars'   => $this->_objectToArray( $viewData ), 
			'_return' => $return
		]);

		// Clear view data else if using load method multiple
		// times the view data will persist and perhaps cause problems.
		$this->_viewData = [];

		return $out;
	}

	// --------------------------------------------------------------------

	/**
	 * Set variables to be available in any view
	 */
	public function setVars( $vars = [], $val = '' )
	{
		// If string and supplied for $vars
		if( is_string( $vars ) )
			$vars = [ $vars => $val ];

		// If object supplied for $vars, convert to array
		if( is_object( $vars ) )
			$vars = $this->_objectToArray( $vars );

		// If array supplied for $vars and array not empty
		if( is_array( $vars ) AND count( $vars ) > 0)
			foreach( $vars as $key => $val )
				$this->_viewData[ $key ] = $val;
	}

	// --------------------------------------------------------------------

	/**
	 * Add View Path
	 */
	public function addPath( $path )
	{
		$this->_setDefaultPaths();

		// Prepend the paths array with the new path
		array_unshift( $this->viewPaths, realpath( $path ) . '/' );
	}

	// --------------------------------------------------------------------

	/**
	 * Set the default paths. First is app/views, second is vendor/isolated/views
	 */
	protected function _setDefaultPaths()
	{
		// The default view directories always need to be loaded first
		if( empty( $this->viewPaths ) )
			$this->viewPaths = [ __DIR__ . '/views/' ];
	}
	
	// -----------------------------------------------------------------------

	/**
	 * Internal view loader
	 */
	protected function _load( $_data )
	{
		$this->_setDefaultPaths();

		// Use processed variables $_view, $_vars, and $_return 
		list( $_view, $_vars, $_return ) = $this->_processLoadData( $_data );

		// Add a file extension the view
		$_file = $_view . '.php';

		// Get the view path
		$_viewPath = $this->_getViewPath( $_file );

		// Display error if view not found
		if ( ! $_viewPath )
			$this->_viewNotFoundError( $_file );

		// Combine view variables
		if( is_array( $_vars ) )
			$this->_viewData = array_merge( $this->_viewData, $_vars );

		// Make view variables available in included view
		extract( $this->_viewData );

		// Start output buffering if returning view
		if( $_return === TRUE )
			ob_start();

		// Include view
		include( $_viewPath );

		// Return processed view if requested
		if( $_return === TRUE )
			return ob_get_clean();
	}

	// --------------------------------------------------------------------

	/**
	 * Object to Array
	 *
	 * Takes an object as input and converts the class variables to array key/vals
	 */
	protected function _objectToArray( $object )
	{
		return ( is_object( $object ) ) 
			? get_object_vars( $object ) 
			: $object;
	}

	// --------------------------------------------------------------------

	/**
	 * Explode data for use in _load method
	 */
	protected function _processLoadData( $data )
	{
		foreach( [ '_view', '_vars', '_return' ] as $val )
		{
			$$val = ( ! isset( $data[ $val ] ) ) 
				? FALSE 
				: $data[ $val ];
		}

		return [ $_view, $_vars, $_return ];
	}
	
	// -----------------------------------------------------------------------

	/**
	 * Get the view path
	 */
	protected function _getViewPath( $file )
	{
		foreach( $this->viewPaths as $viewDir )
			if( file_exists( $viewDir . $file ) )
				return $viewDir . $file;

		return FALSE;
	}
	
	// -----------------------------------------------------------------------

	/**
	 * Display error when no view found
	 */
	protected function _viewNotFoundError( $_file )
	{
		$errText = PHP_EOL . 
			'View file "' . $_file . '" not found.' . PHP_EOL . 
			'Directories checked: ' . PHP_EOL . 
			'[' . implode( '],' . PHP_EOL . '[', $this->viewPaths ) . ']' . PHP_EOL;

		throw new \Exception( $errText );
	}
	
	// -----------------------------------------------------------------------

	/**
	 * Get view data for debugging
	 */
	public function getViewData()
	{
		return $this->_viewData;
	}
	
	// -----------------------------------------------------------------------

}
