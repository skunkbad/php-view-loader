<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require __DIR__ . '/../../vendor/autoload.php';

$v = new Skunkbad\ViewLoader\View;
$v->addPath( __DIR__ . '/../views/' );
$v->load('with-variable', [
	'variable' => $v->load('with-variable', [
		'variable' => $v->load('with-two-variables', [
			'variable1' => 'penguins',
			'variable2' => $v->load('simple', NULL, TRUE) . 'sharks'
		], TRUE)
	], TRUE)
]);

// Expected
echo '<br>withwithwithpenguinsandsimplesharks';