<?php

use PHPUnit\Framework\TestCase;
use Skunkbad\ViewLoader\View;

class CoreTest extends TestCase {

	public function testLoadSimpleView()
	{
		$v = new View;
		$v->addPath( __DIR__ . '/views/' );
		$str = $v->load('simple', NULL, TRUE);

		$this->assertEquals( 'simple', $str );
	}
	
	// -----------------------------------------------------------------------

	public function testLoadViewWithVariable()
	{
		$v = new View;
		$v->addPath( __DIR__ . '/views/' );
		$str = $v->load('with-variable', [
			'variable' => 'unicorns'
		], TRUE);

		$this->assertEquals( 'withunicorns', $str );
	}
	
	// -----------------------------------------------------------------------

	public function testLoadNestedView()
	{
		$v = new View;
		$v->addPath( __DIR__ . '/views/' );
		$str = $v->load('with-variable', [
			'variable' => $v->load('simple', NULL, TRUE)
		], TRUE);

		$this->assertEquals( 'withsimple', $str );
	}
	
	// -----------------------------------------------------------------------

	public function testLoadMultiNestedViews()
	{
		$v = new View;
		$v->addPath( __DIR__ . '/views/' );
		$str = $v->load('with-variable', [
			'variable' => $v->load('with-variable', [
				'variable' => $v->load('with-two-variables', [
					'variable1' => 'penguins',
					'variable2' => $v->load('simple', NULL, TRUE) . 'sharks'
				], TRUE)
			], TRUE)
		], TRUE);

		$this->assertEquals( 'withwithwithpenguinsandsimplesharks', $str );
	}
	
	// -----------------------------------------------------------------------

	public function testSetVars()
	{
		$v = new View;
		$v->addPath( __DIR__ . '/views/' );
		$v->setVars('variable', 'unicorns');
		$str = $v->load('with-variable', NULL, TRUE);

		$this->assertEquals( 'withunicorns', $str );
	}
	
	// -----------------------------------------------------------------------

	/**
     * @expectedException Exception
     */
	public function testExceptionThrownWhenViewFileDoesNotExist()
	{
		$v = new View;
		$v->load('lost-file', NULL, TRUE);
	}
	
	// -----------------------------------------------------------------------

}