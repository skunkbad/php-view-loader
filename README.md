# PHP View Loader

This package allows for loading traditional PHP views without any dependencies.

Simple Example:

```php
<?php
// Loads a view named page-content.php and passes 
// in the current user, available as $currentUser.
$view = new \Skunkbad\ViewLoader\View;
$view->addPath( __DIR__ . '/views/' );
$view->load('page-content', [
	'currentUser' => $currentUser
]);
```

Return the same view, instead of outputting it:

```php
<?php
$str = $view->load('page-content', [
	'currentUser' => $currentUser
], TRUE );
```
